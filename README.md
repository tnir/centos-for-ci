# test cases for using CentOS 6/7/8 for CI

### Author

[Taku Noguchi](mailto:tak.noguchi.iridge@gmail.com)

### License

This code is distributed under the MIT license, see the LICENSE file.
